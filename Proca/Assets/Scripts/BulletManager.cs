﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class BulletManager : MonoBehaviour
    {
        [SerializeField] private GameObject bulletPrefab;
        [SerializeField] private CameraMovementScript cameraMovementScript;

        private void Start()
        {
            InstatiateBullet();
            cameraMovementScript.ResetCameraCalled += OnResetCameraCalled;
        }

        public void InstatiateBullet()
        {
            var prefab = Instantiate(bulletPrefab, this.transform, false);
            prefab.GetComponent<BulletScript>().CameraMovementScript = cameraMovementScript;
        }

        private void OnResetCameraCalled()
        {
            InstatiateBullet();
        }

        private void OnDestroy()
        {
            if (cameraMovementScript != null){
                cameraMovementScript.ResetCameraCalled -= OnResetCameraCalled;
            }
        }
    }
}
