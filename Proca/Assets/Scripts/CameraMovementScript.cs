﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class CameraMovementScript : MonoBehaviour
    {
        public event Action ResetCameraCalled = delegate { };

        private Transform _objectToTrack;
        private Vector3 _startPosition;


        private void Start()
        {
            _startPosition = transform.position;
        }

        private void Update()
        {
            if (_objectToTrack != null)
            {
                transform.position = new Vector3(_objectToTrack.position.x, transform.position.y, _startPosition.z);
            }
        }

        public void ResetCamera()
        {
            _objectToTrack = null;

            ResetCameraCalled();
            this.transform.position = _startPosition;
        }

        public void TrackObject(Transform objectToTrack)
        {
            _objectToTrack = objectToTrack;
        }
    }
}
