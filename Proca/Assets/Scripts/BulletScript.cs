﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{

    public CameraMovementScript CameraMovementScript { get; set; }

    private Camera _camera;
    private Rigidbody2D _rigidBody;

    private Vector2 _startPosition;

    private Vector2 _force;
    private float _xForceMultiplier = 150f;
    private float _yForceMultiplier = 200f;

    private bool _skipCollisioncheck;
    void Start()
    {

        _rigidBody = GetComponent<Rigidbody2D>();
        _startPosition = transform.position;
        _camera = Camera.main;
        _skipCollisioncheck = false;
    }


    void OnMouseDrag()
    {
        var xRangeDiff = 1.5f;
        var yRangeDiff = 0.5f;

        var mousePosition = _camera.ScreenToWorldPoint(Input.mousePosition);
        var x = Mathf.Clamp(mousePosition.x, _startPosition.x - xRangeDiff, _startPosition.x);
        var y = Mathf.Clamp(mousePosition.y, _startPosition.y - yRangeDiff, _startPosition.y + yRangeDiff);
        _rigidBody.MovePosition(new Vector2(x, y));

        var xForce = _xForceMultiplier * Math.Abs(x - _startPosition.x);
        var yForce = _yForceMultiplier * Math.Abs(y - _startPosition.y);
        _force = new Vector2(xForce, yForce);
    }

    private void OnMouseUp()
    {
        _rigidBody.gravityScale = 0.6f;
        _rigidBody.AddForce(_force);
        CameraMovementScript.TrackObject(this.transform);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.collider.name.Equals("Ground") || _skipCollisioncheck) return;
        _skipCollisioncheck = true;
        StartCoroutine(ResetCoroutine());
    }

    private IEnumerator ResetCoroutine()
    {
        yield return new WaitForSeconds(1f);
        _rigidBody.velocity = Vector2.zero;
        CameraMovementScript.ResetCamera();
    }
}
