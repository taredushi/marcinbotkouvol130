﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class ObjectPool : MonoBehaviour
    {
        [SerializeField] private GameObject ballPrefab;

        public List<GameObject> pooledObjects;

        private void Start()
        {
            pooledObjects = new List<GameObject>();    
        }

        public GameObject GetObject()
        {
            foreach(var gameObject in pooledObjects)
            {
                if (!gameObject.activeSelf)
                {
                    gameObject.SetActive(true);
                    return gameObject;
                }
            }

            var prefab = Instantiate(ballPrefab, transform, false);
            pooledObjects.Add(prefab);
            return prefab;
        }
    }
}
