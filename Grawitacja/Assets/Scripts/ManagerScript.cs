﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagerScript : MonoBehaviour {

    [SerializeField] private ObjectPool objectPool;
    [SerializeField] private Text counterText;


    private int _counter;
    private WaitForSeconds _waitInitialize;
    private Camera _camera;

	// Use this for initialization
	void Start () {
        _waitInitialize = new WaitForSeconds(1 / 4f);
        _camera = Camera.main;
        StartCoroutine(InitializeBallCoroutine());
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private IEnumerator InitializeBallCoroutine()
    {
        while(_counter < 250)
        {
            yield return _waitInitialize;
            var ball = objectPool.GetObject();

            var x = Random.Range(-_camera.orthographicSize, _camera.orthographicSize);
            var y = Random.Range(-_camera.orthographicSize, _camera.orthographicSize);

            ball.transform.position = new Vector3(x, y);
            IncreaseCounter();
        }
    }

    private void IncreaseCounter()
    {
        _counter++;
        counterText.text = _counter.ToString();
    }
}
